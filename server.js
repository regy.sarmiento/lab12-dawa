require("./config/config")

const express = require("express");
const mongoose = require('mongoose')

//var usuario = require('./routes/usuario');

const app = express();

app.use(express.static(__dirname + '/public'));


const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());


app.use(require('./routes/index.js'));


console.log("=================================================");
console.log(process.env.NODE_ENV);
console.log(process.env.URLDB);
console.log("=================================================");


mongoose.connect(process.env.URLDB,{useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true },
                                    (err, res) => {
                                        if (err) throw err;
                                        console.log("Base de datos Online");
                                    }
);



/*
app.post('/usuario', usuario.usuarioPost);
app.get('/usuario', usuario.usuarioGet);
app.put('/usuario/:id', usuario.usuarioPut);
app.delete('/usuario/:id', usuario.usuarioDelete);

app.get('/', usuario.index);

app.post('/validarCrear',usuario.validarCrear);
*/
////////////////////////lab09




app.listen(process.env.PORT, () => console.log(`escuchando peticiones en puerto ${process.env.PORT}`));
